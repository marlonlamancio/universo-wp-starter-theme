<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div id="page" class="site">

		<!-- Navegação principal do site (Navbar) -->
		<div id="wrapper-navbar">

			<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Ir para o conteúdo', 'universowp' ); ?></a>

			<nav class="navbar navbar-expand-md navbar-dark bg-primary">

				<!-- Área da logomarca -->
				<?php if ( ! has_custom_logo() ) : ?>

					<?php if ( is_front_page() && is_home() ) : ?>

						<h1 class="navbar-brand mb-0">
							<a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
								<?php bloginfo( 'name' ); ?>
							</a>
						</h1>

					<?php else : ?>

						<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
							<?php bloginfo( 'name' ); ?>
						</a>

					<?php endif; ?>

				<?php else : ?>

					<?php the_custom_logo(); ?>
				
				<?php endif; ?>
				<!-- Fim da logo -->

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#wrapper-navbar-nav" aria-controls="wrapper-navbar-nav" aria-expanded="false" aria-label="<?php esc_attr_e( 'Alternar Navegação', 'universowp' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<!-- Menu Principal -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'wrapper-navbar-nav',
						'menu_class'      => 'navbar-nav ml-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'primary-menu',
						'depth'           => 2,
						'walker'          => new WP_Bootstrap_Navwalker(),
						'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
					)
				); ?>

			</nav><!-- .site-navigation -->

		</div><!-- #wrapper-navbar end -->

		<div id="wrapper-site-content">

			<div id="content" class="site-content">
