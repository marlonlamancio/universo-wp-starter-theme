<?php
/**
 * Arquivo de configurações e funções do tema.
 *
 * Esse arquivo se comporta como um plugin do WordPress adicionando recursos e funcionalidades ao site.
 *
 * É aqui que definimos as configurações do tema e suas funcionalidades específicas.
 *
 * Por exemplo iremos configurar quais recursos o tema irá suportar, como por exemplo: suporte a HTML5, Post Thumbnails, Title Tag etc.
 * Veja a função wplaunch_setup_theme().
 *
 * Iremos definir as áreas de widgets e menus do tema.
 *
 * É também aqui que deveremos requisitar outros arquivos do site como CSS e JavaScript.
 * Veja a função wplaunch_enqueue_scripts().
 *
 * Além de definir outras funções que serão usadas ao longo do tema, como por exemplo tags de templates criadas especificamente para os templates do nosso tema.
 *
 * Aqui também podemos alterar o comportamento padrão do WordPress através dos ganchos (hooks – actions e filters) que o WordPress disponibiliza durante seu ciclo de execução.
 *
 * Para mais informações sobre hooks, actions, e filters,
 * veja http://codex.wordpress.org/Plugin_API
 *
 * @package universowp
 * @since 0.0.1
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$universowp_includes = array(
	'/helpers.php',							// Funções auxiliares no desenvolvimento do tema
	'/theme-setup.php',                     // Setup e registro de suporte aos recursos do tema
	'/widgets.php',                         // Registra as áreas de widgets do tema
	'/class-wp-bootstrap-navwalker.php',    // Adapta o menu do WordPress (nav walker) para o Boostrap 4
	'/enqueue-assets.php',                  // Carrega os arquivos JS e CSS
	'/template-tags.php',                   // Tempate Tags do tema
);

foreach ( $universowp_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}
