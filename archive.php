<?php
/**
 * Template responsável por exibir as páginas de arquivos de posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package universowp
 */

get_header();
?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Início do Loop */
			while ( have_posts() ) :

				the_post();

				/*
				 * Inclui o template para exibir o conteúdo específico do tipo de Post
				 * Se quiser sobrescrever esse template em um tema-filho, então inclua um arquivo
				 * com o nome content-____.php (substituindo ____ pelo nome do tipo de Post personalizado)
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
		
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
