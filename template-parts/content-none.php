<?php
/**
 * Parte de Template para exibir uma mensagem de que não foi encontrado nenhum post para a busca
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package universowp
 */

?>

<section class="no-results not-found">

	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Não foi encontrado nenhum conteúdo', 'universowp' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Pronto para publicar seu primeiro post? <a href="%1$s">Comece aqui</a>.', 'universowp' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>

			<p><?php esc_html_e( 'Desculpe, mas não foi encontrado nada correspondente aos seus termos de pesquisa. Por favor, tente novamente com algumas palavras-chave diferentes.', 'universowp' ); ?></p>
			<?php
			get_search_form();

		else :
			?>

			<p><?php esc_html_e( 'Não conseguimos encontrar o que você está procurando. Tente no campo de busca abaixo.', 'universowp' ); ?></p>
			<?php
			get_search_form();

		endif;
		?>
	</div><!-- .page-content -->
	
</section><!-- .no-results -->
