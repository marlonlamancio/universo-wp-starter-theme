<?php
/*
 * Confere se a constante SCRIPT_DEBUG está ativada e armazena
 * na variável global $script_suffix o sufixo a ser usado para carregar
 * a versã dos scritps corretamente.
 *
 * Dessa forma conseguimos carregar os arquivos otimizados no ambiente de produção,
 * e os arquivos expandidos no ambiente de desenvolvimento.
 *
 * @since 0.0.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Sair se esse arquivo for acessado diretamente
}

// Define se irá carregar versão de dev ou produção dos scripts
$script_suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

if ( ! function_exists( 'universowp_enqueue_assets' ) ) {

	/**
	 * Carrega os JavaScripts e estilos CSS do site
	 *
	 * @since 0.1.0
	 */
	function universowp_enqueue_assets() {

		global $script_suffix;

		// Pega informações do tema
		$universowp_template_url = get_template_directory_uri();
		$universowp_theme = wp_get_theme();

		// Carrega o arquivo CSS principal do tema
		wp_enqueue_style( 'universowp-style', get_stylesheet_uri(), array(), null, 'all' );

		// Carrega JS do Bootstrap
		wp_enqueue_script( 'bootstrap-js-bundle', $universowp_template_url . '/assets/js/bootstrap.bundle' . $script_suffix  . '.js', array( 'jquery' ), '4.3.1', true );

		// Carrega arquivo JS gerado e otimizado pelo Gulp
		//wp_enqueue_script( 'universowp-js', $universowp_template_url . '/assets/js/scripts' . $script_suffix  . '.js', array( 'jquery' ), $universowp_theme->get( 'Version' ), true );

		// Script de comentários
		if ( is_single() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

	}

}

add_action( 'wp_enqueue_scripts', 'universowp_enqueue_assets' );

if ( ! function_exists( 'universowp_change_stylesheet_uri' ) ) {

	/**
	 * Altera a URI do arquivo CSS principal do tema
	 *
	 * @since  0.0.1
	 *
	 * @param  string $uri URI Padrão.
	 * @param  string $dir Diretório (URI) da folha de estilos CSS.
	 *
	 * @return string      Nova URI.
	 */
	function universowp_change_stylesheet_uri( $uri, $dir ) {
		global $script_suffix;
		return $dir . '/assets/css/style' . $script_suffix . '.css';
	}

}

add_filter( 'stylesheet_uri', 'universowp_change_stylesheet_uri', 10, 2 );
