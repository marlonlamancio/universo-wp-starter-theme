<?php
/**
 * Arquivo responsável por registrar todas áreas de widgets como Sidebars, Rodapé etc.
 *
 * @package universowp
 */

function universowp_widgets_init() {

  /*
   * Sidebar barra lateral (uma área de widget)
   */
  register_sidebar( array(
    'name'            => __( 'Sidebar', 'universowp' ),
    'id'              => 'sidebar-widgets-right',
    'description'     => __( 'Sidebar da barra lateral direita', 'universowp' ),
    'before_widget'   => '<section id="%1$s" class="widget %2$s">',
    'after_widget'    => '</section>',
    'before_title'    => '<h3 class="widget-title">',
    'after_title'     => '</h3>',
  ) );

  /*
   * Rodapé (1, 2, 3, ou 4 widgets)
   *
   * Flexbox `col-sm` irá fornecer a correta largura da coluna:
   *
   * Se tiver somente 1 widget então o widget terá a largura total.
   * Se tiverem 2 widgets cada um terá metada de largura total.
   * Se tiverem 3 widgets cada um terá 1/3 da largura total.
   * Se forem 4 widgets então cada um terá 1/4 da largura total.
   *
   * Isso à partir do ponto de quebra (breakpoint) 'sm' do Bootstrap.
   */

  register_sidebar( array(
    'name'            => __( 'Rodapé', 'universowp' ),
    'id'              => 'footer-widgets',
    'description'     => __( 'Área de widgets do rodapé', 'universowp' ),
    'before_widget'   => '<div id="%1$s" class="widget %2$s col-sm">',
    'after_widget'    => '</div>',
    'before_title'    => '<h3 class="widget-title">',
    'after_title'     => '</h3>',
  ) );

}
add_action( 'widgets_init', 'universowp_widgets_init' );
