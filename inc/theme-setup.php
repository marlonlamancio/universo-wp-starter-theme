<?php

/**
 * Define as configurações padrões do tema e o suporte a vários recursos do WordPress.
 *
 * Note que esta função é conectada ao gancho after_setup_theme,
 * que é executado antes do gancho init.
 * O gancho init é muito tarde para alguns recursos,
 * como por exemplo indicar suporte para post thumbnails.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Sair se esse arquivo for acessado diretamente
}

function universowp_setup_theme() {

	/*
	* Recurso do WordPress para definir a largura máxima permitida de mídias inseridas no conteúdo dos posts.
	*
	* $content_width é uma variável global usada pelo WordPress para definir o tamanho máximo permitido
	* para qualquer conteúdo inserido no front-end do site, como imagens adicionadas às postagens
	* e incorporação de mídias (oEmbed).
	*
	* Isso é importante pois dessa forma o WordPress pode escalar o código de oEmbed
	* para um tamanho específico (largura) no front-end e inserir imagens grandes
	* sem quebrar a área de conteúdo principal.
	* Além disso os plugins podem integrar-se perfeitamente ao tema, já que os mesmos
	* podem acessar o valor armazenado em $content_width.
	*
	* Definimos 1140px por padrão pois é a largura máxima do container no Bootstrap
	*
	* Saiba mais https://codex.wordpress.org/Content_Width
	*/
	if ( ! isset( $content_width ) ) {
		$content_width = 640;
	}

	/*
	 * Torna o tema disponível para tradução.
	 *
	 * Nesse tema decidimos manter as strings e textos em português
	 * pois nosso foco é o desenvolvedor Brasileiro.
	 *
	 * Os arquivos de tradução deverão ficar no diretório /languages/.
	 *
	 * Como o objetivo desse tema é para ser customizado e servir de base
	 * para a criação de seu próprio tema, recomendamos alterar
	 * todas referências ao nome do tema 'universowp' nos templates e funções do tema.
	 *
	 * Se estiver criando um tema filho não tem necessidade de alterar as referências
	 * ao nome do tema 'wplaunc', você deverá usar a função load_child_theme_textdomain().
	 *
	 * O nome das funções, classes e outras nomenclaturas relativas ao código
	 * estão em inglês mas não tem nada a ver com esse arquivo de tradução.
	 * Achamos o inglês mais expressivo e ao mesmo tempo
	 * mais curto de descrever as funções do que o português.
	 *
	 * Para saber mais sobre internacionalização e tradução acesse https://developer.wordpress.org/themes/functionality/internationalization/
	 */
	load_theme_textdomain( 'universowp', get_template_directory() . '/languages' );

	/*
	* Adiciona a tag do link de feed RSS padrão no <head> do tema.
	*/
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Permite o próprio WordPress adicionar a tag <title> no documento HTML.
	 *
	 * Ao declarar o suporte a title-tag nós indicamos que o tema não coloca a
	 * metatag <title> no <head> manualmente, mas deixamos para o WordPress
	 * ou plugins (como por exemplo o Yoast SEO) fornecer para nós automaticamente.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Habilita suporte para Post Thumbnails em posts e páginas.
	 *
	 * Post Thumbnails, também conhecido como Featured Images (Imagem em destaque)
	 * é um recurso do WordPress que permite você escolher uma imagem para representar
	 * um post, ou página ou tipo de conteúdo personalizado (Custom Post Type) nos templates
	 * e páginas do site.
	 *
	 * Por padrão o tamanho do thumbnail é 150x150 pixels.
	 * Se quiser alterar esse valor use a função set_post_thumbnail_size(), exemplo:
	 * set_post_thumbnail_size( 300, 300 ); // 300 pixels de largura por 300 pixels de altura em modo de redimensionamento.
	 *
	 * Se quiser adicionar o suporte a esse recurso para um tipo de conteúdo customizado (Custom Post Type)
	 * você pode passar um segundo argumento com um array dos tipos de conteúdo que você deseja habilitar.
	 * Exemplo: add_theme_support( 'post-thumbnails', array( 'post', 'portfloio' ) ); // Post padrão e o Custom Post Type Portfolio
	 * Lembrando que nesse exemplo o tipo de conteúdo Portfolio deverá ser criando com a função register_post_type().
	 *
	 * Saiba mais em https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Altera a marcação HTML padrão do formulário de pesquisa,
	 * do formulário de comentários e os comentários,
	 * da galeria e legendas de imagens,
	 * para uma saída de HTML5 válida.
	 *
	 * Por que isso é importante?
	 * Torna a marcação HTML do tema mais semântica.
	 * Por exemplo, ao invés de usar um elemento HTML genérico para a legenda das imagens
	 * irá usar o elemento específico <figcaption>.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Adiciona suporte para conteúdo incorporado responsivo.
	add_theme_support( 'responsive-embeds' );

	/* Esse recurso permite a atualização seletiva de widgets gerenciados no Customizer.
	 * Customizer é a forma padrão hoje no WordPress de personalizar os temas.
	 */
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * Post Formats: https://codex.wordpress.org/Post_Formats
	 * Custom Background: https://codex.wordpress.org/Custom_Backgrounds
	 * Custom Header: https://codex.wordpress.org/Custom_Headers
	 * Custom Logo: https://codex.wordpress.org/Theme_Logo
	 * Ou use o gerador de códigos https://generatewp.com/
	 */

	/*
	 * Habilita suporte para Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Suporte ao recurso de logo customizada
	add_theme_support( 'custom-logo' );

}

add_action( 'after_setup_theme', 'universowp_setup_theme' );

/*
 * Registra os menus a serem usados no site.
 *
 * Para usar o recurso de menus no tema primeiro é necessário registrar o menu com a função register_nav_menus()
 * para o menu aparecer ba área administrativa do site e podermos adicionar posts, páginas, categorias etc.
 * Depois precisamos informar ao tema onde iremos exibí-lo com a função wp_nav_menu() no respectivo template.
 *
 * Abaixo registramos o primary-menu e iremos exibí-lo no template de cabeçalho (primary.php) do tema.
 * Deixamos um menu extra comentado caso queira registrar mais um menu no tema.
 */
function universowp_register_menus() {

	register_nav_menus( array(
		'primary'  => esc_html__( 'Menu Primário', 'universowp' ), // Menu principal no cabeçalho do site
		//'menu-extra'   => esc_html__( 'Menu Extra', 'universowp' ), // Menu extra de exemplo
	) );

}

add_action( 'init', 'universowp_register_menus' );
