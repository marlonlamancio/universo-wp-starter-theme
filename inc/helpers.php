<?php
/**
 * Arquivo com funções auxiliares
 *
 * @package universowp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Sair se esse arquivo for acessado diretamente
}

/** Função para auxiliar a debugar escrevendo no arquivo de log em /content/debug.log **/
if ( ! function_exists('write_log')) {
	function write_log ( $log )  {
		if ( is_array( $log ) || is_object( $log ) ) {
			error_log( print_r( $log, true ) );
		} else {
			error_log( $log );
		}
	}
}