<?php
/**
 * Template para exibir visualização dos posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package universowp
 */

get_header();
?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation();

			// Se os comentários estiverem abertos ou se tivermos pelo menos um comentário, carregue o template de comentários.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // Fim do loop.
		?>

		</main><!-- #main -->

	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
