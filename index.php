<?php
/**
 * Este é o arquivo principal de template e o fallback do tema.
 *
 * Este é o arquivo de template mais genérico em um tema do WordPress e um dos dois arquivos necessários para um tema (o outro é style.css).
 * É usado para exibir uma página quando nada mais específico corresponde a uma consulta.
 * Por exemplo, ele exibe a home page quando não existe um arquivo home.php.
 *
 * @package universowp
 */

get_header();
?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Início do Loop */
			while ( have_posts() ) :
				
				the_post();

				/*
				 * Inclui o template para exibir o conteúdo específico do tipo de Post
				 * Se quiser sobrescrever esse template em um tema-filho, então inclua um arquivo
				 * com o nome content-____.php (substituindo ____ pelo nome do tipo de Post personalizado)
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->

	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();