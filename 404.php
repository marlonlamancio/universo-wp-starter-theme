<?php
/**
 * Template para exibir página de erro 404 (página não encontrada)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package universowp
 */

get_header();
?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

			<section class="error-404 not-found">

				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Oops! A página requisitada não foi encontrada.', 'universowp' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">

					<p><?php esc_html_e( 'Nenhum conteúdo foi encontrado. Tente um dos links abaixo ou faça uma pesquisa.', 'universowp' ); ?></p>

					<?php
					get_search_form();

					the_widget( 'WP_Widget_Recent_Posts' );
					?>

					<div class="widget widget_categories">

						<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'universowp' ); ?></h2>
						
						<ul>
							<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
							?>
						</ul>

					</div><!-- .widget -->

					<?php
					/* translators: %1$s: smiley */
					$universowp_archive_content = '<p>' . sprintf( esc_html__( 'Tente procurar nos arquivos mensais. %1$s', 'universowp' ), convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$universowp_archive_content" );

					the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div><!-- .page-content -->

			</section><!-- .error-404 -->

		</main><!-- #main -->

	</div><!-- #primary -->

<?php
get_footer();
