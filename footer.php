<?php
/**
 * Template para exibir o rodapé do site
 *
 * Conteúdo à partir da tag div #content de fechamento do conteúdo principal e toda marcação que vem depois
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package universowp
 */

?>

			</div><!-- #content -->

		</div><!-- #wrapper-site-content -->

		<footer id="colophon" class="site-footer">

			<div class="site-info">
				<a href="<?php echo esc_url( __( 'https://universowp.com.br/', 'universowp' ) ); ?>">
					<?php
					/* translators: %s: Nome do CMS, i.e. WordPress. */
					printf( esc_html__( 'Orgulhosamente feito com %s', 'universowp' ), 'WordPress' );
					?>
				</a>
				<span class="sep"> | </span>
					<?php
					/* translators: 1: Nome do tema, 2: Nome do autor. */
					printf( esc_html__( 'Tema: %1$s por %2$s.', 'universowp' ), 'Universo WP Starter Theme', '<a href="https://universowp.com.br/">Universo WP</a>' );
					?>
			</div><!-- .site-info -->

		</footer><!-- #colophon -->

	</div><!-- #page -->

	<?php wp_footer(); ?>

</body>
</html>