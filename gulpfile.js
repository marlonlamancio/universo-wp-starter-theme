'use strict';

//// CARREGA TODOS PLUGINS NODE NECESSÁRIOS ////

const gulp    = require('gulp'), // Carrega o Gulp
	  plumber = require( 'gulp-plumber' ), // Previne a quebra do fluxo de dados (pipe) causado por erros dos plugins do Gulp
	  sass    = require('gulp-sass'); // Carrega o plugin necessário para compilar os arquivos .SCSS

//// DEFINE ALGUMAS CONSTANTES DE CONFIGURAÇÃO PARA AUXILIAR ////

const paths = {
	node: './node_modules/',
	assets: {
		css: './assets/css/',
		fonts: './assets/fonts/',
		images: './assets/images/',
		js: './assets/js/',
		sass: './assets/sass/',
		vendors: './assets/vendors/'
	}
};

//// PROCESSA OS ARQUIVOS .SCSS ////

function scss() {
	return gulp.src(paths.assets.sass + 'style.scss')
		.pipe(
			plumber({
				errorHandler: function(err) {
					console.log(err);
					this.emit('end');
				}
			})
		)
		.pipe(sass({ errLogToConsole: true }))
		.pipe(gulp.dest(paths.assets.css));
}

//// VIGIA AS ALTERAÇÕES NOS ARQUIVOS EM DESENVOLVIMENTO PARA PROCESSÁ-LOS ////

function watcher() {
	gulp.watch(paths.assets.sass, scss);
};

//// COPIA TODOS OS ATIVOS DE TERCEIROS NECESSÁRIOS ////

function copyVendorsAssets(done) {
	
	//// Copiar arquivos fonte do Bootstrap ////

	// Copiar todos arquvios SCSS do Bootstrap
	gulp.src(paths.node + '/bootstrap/scss/**/*.scss')
	.pipe(gulp.dest(paths.assets.vendors + '/bootstrap/scss/'));

	// Copiar todos arquvios JS do Bootstrap
	gulp.src(paths.node + '/bootstrap/dist/js/bootstrap.bundle*.js')
	.pipe(gulp.dest(paths.assets.js));

	//// Copiar arquivos fonte do FontAwesome ////

	// Copiar arquivos SCSS do FontAwesome
	gulp.src(paths.node + '/@fortawesome/fontawesome-free/scss/**/*.scss')
	.pipe(gulp.dest(paths.assets.vendors + '/fontawesome-free/scss/'));

	// Copiar webfonts do FontAwesome
	gulp.src(paths.node + '/@fortawesome/fontawesome-free/webfonts/**')
	.pipe(gulp.dest(paths.assets.fonts));

	done();
}

exports.scss = scss;
exports.copyVendorsAssets = copyVendorsAssets;
exports.default = watcher;
