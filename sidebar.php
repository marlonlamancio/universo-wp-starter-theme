<?php
/**
 * Sidebar contendo a principal área de widget
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package universowp
 */

if ( ! is_active_sidebar( 'sidebar-widgets-right' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-widgets-right' ); ?>
</aside><!-- #secondary -->
